<?php

/*

 * Template Name: Page with left sidebar

 */

?>

<?php get_header(); ?>
	
	<div id="content">
	
		<div id="inner-content" class="grid-x">
    
            <?php get_sidebar(); ?>
	
		    <main id="main" class="large-9 medium-8 small-12 cell" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->

		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>