<?php
/*
 * Template Name: php info template
 */
?>
<?php get_header(); ?>
	
	<div id="content">
	
		<div id="inner-content" class="grid-x">
	
		    <main id="main" class="small-12 cell" role="main">
				
			<?php phpinfo() ?>			
			    					
			</main> <!-- end #main -->

		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>