<?php

/*

 * Template Name: Four sidebar columns

 */

?>

<?php get_header(); ?>

<div id="content">

  <div id="inner-content" class="grid-x">

 
    <?php the_content(); ?>

    <div id="main" class="grid-x" role="main">

          <?php dynamic_sidebar("Sidebar 4 1"); ?>
          <?php dynamic_sidebar("Sidebar 4 2"); ?>
          <?php dynamic_sidebar("Sidebar 4 3"); ?>
          <?php dynamic_sidebar("Sidebar 4 4"); ?>
    </div>
    <!-- end #main -->

  </div>
  <!-- end #inner-content -->

</div>
<!-- end #content -->

<?php get_footer(); ?>
