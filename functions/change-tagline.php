<?php
// create new change_tagline action hook
add_action('RFKchange_tagline', 'RFKchange_description');

// function to schedule event if it doesn't exist
function RFKactivate_change() {
    // check if change_tagline action/event already exists
    if ( !wp_next_scheduled( 'RFKchange_tagline' ) ) {
        // schedule new event to trigger change_tagline daily
      wp_schedule_event( time(), 'hourly', 'RFKchange_tagline');
      error_log("scheduled rfKchange_tagline");
	}
}

// call the change after global WP class object is set up
add_action('wp', 'RFKactivate_change');

// function to change the tagline/description
function RFKchange_description() {
    // array of descriptions, anything you want
    $description_list = array(
        "Jesus, I trust in You",
        "This is my beloved Son, listen to Him",
        "This is my body given for you",
        "I am the way, and the truth, and the life",
        "I am the resurrection and the life",
        "I am the vine, you are the branches",
        "I am the resurrection and the life",
        "I am the bread of life",
        "I am the light of the world",
        "Father, forgive them; for they know not what they do",
        "Father, into thy hands I commend my spirit"
    );
    // pick new description from array
    $new_description = $description_list[array_rand($description_list)];
    // update site tagline (blogdescription) in database
    update_option( 'blogdescription', $new_description);
    error_log($new_description);
}
?>