<?php
// SIDEBARS AND WIDGETIZED AREAS
function joints_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'jointswp'),
		'description' => __('The first (primary) sidebar.', 'jointswp'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar(array(
		'id' => 'offcanvas',
		'name' => __('Offcanvas', 'jointswp'),
		'description' => __('The offcanvas sidebar.', 'jointswp'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'jointswp'),
		'description' => __('The second (secondary) sidebar.', 'jointswp'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
	register_sidebar ( array (
	    'name' => 'Sidebar 4 1',
	    'id' => 'sidebar41',
	    'before_widget' => '<div class="small-12 large-3 cell"><article id="%1$s" class="widget %2$s">',
	    'after_widget' => '</article></div>',
	    'before_title' => '<h4>',
	    'after_title' => '</h4>'
	) );
	
	register_sidebar ( array (
	    'name' => 'Sidebar 4 2',
	    'id' => 'sidebar42',
	    'before_widget' => '<div class="small-12 large-3 cell"><article id="%1$s" class="widget %2$s">',
	    'after_widget' => '</article></div>',
	    'before_title' => '<h4>',
	    'after_title' => '</h4>'
	) );
	register_sidebar ( array (
	    'name' => 'Sidebar 4 3',
	    'id' => 'sidebar43',
	    'before_widget' => '<div class="small-12 large-3 cell"><article id="%1$s" class="widget %2$s">',
	    'after_widget' => '</article></div>',
	    'before_title' => '<h4>',
	    'after_title' => '</h4>'
	) );
	register_sidebar ( array (
	    'name' => 'Sidebar 4 4',
	    'id' => 'sidebar44',
	    'before_widget' => '<div class="small-12 large-3 cell"><article id="%1$s" class="widget %2$s">',
	    'after_widget' => '</article></div>',
	    'before_title' => '<h4>',
	    'after_title' => '</h4>'
	) );
} /* end register sidebars */

add_action( 'widgets_init', 'joints_register_sidebars' );