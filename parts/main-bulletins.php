<div class="front-panel large-4 medium-6 small-12 cell data-equalizer-watch">    <h4>Bulletins</h4>    <h5>Current bulletin</h5>    <a      href="      <?php
    $bulletins = new WP_Query('category_name=bulletins&posts_per_page=1');
    while ($bulletins->have_posts()) :
        $bulletins->the_post();
        the_permalink();
        ?>">
       <?php  the_title(); endwhile;?></a>    <h5>All bulletins</h5>    <?php
    // Get the ID of a given category
    $category_id = get_category_by_slug("bulletins");
    // Get the URL of this category
    $category_link = get_category_link($category_id);
    ?>        <!-- Print a link to this category -->    <a href=" <?php echo esc_url( $category_link ) ; ?> ">All bulletins</a></div>