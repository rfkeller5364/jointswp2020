<div class="large-6 medium-6 small-12 cell">  <div class="front-panel">    <h4>Recent posts</h4>      <?php
    $recent_blog = new WP_Query('category_name=frontpage&posts_per_page=5');
    while ($recent_blog->have_posts()) :        $recent_blog->the_post();
        get_template_part( 'parts/loop', 'smfrontpage' );      endwhile;?></a>    <?php
    // Get the ID of a given category
    $category_id = get_category_by_slug("frontpage");
    // Get the URL of this category
    $category_link = get_category_link($category_id);
    ?>        <!-- Print a link to this category -->    <a href=" <?php echo esc_url( $category_link ) ; ?> ">All posts</a>  </div></div>