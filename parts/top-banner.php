<div class="banner ">
  <?php if (strpos(get_home_url(), 'localhost') == false) : ?>
  <h1>St. John the Baptist Catholic Church</h1>
  <h3>El Cerrito, Calif.</h3>
  <h2 class="show-for-large"> </h2>
  <h2 class="show-for-large"><?php bloginfo('description'); ?></h2>
  <?php else: ?> 
  <h1>Localhost test site</h1>
  <h3>Richard's place</h3>
  <h2 class="show-for-large">Test this well</h2>
  <h2 class="show-for-large"><?php bloginfo('description'); ?></h2>
  <?php endif ?> 
</div>