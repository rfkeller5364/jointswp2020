<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?>
  role="article">
  <header class="article-header">
    <h4>
      <a href="<?php the_permalink() ?>" rel="bookmark"
        title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
    </h4>
 
  </header>

  <section class="entry-content" itemprop="articleBody">
    <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('post-thumbnail'); ?></a>
		<?php the_excerpt(); ?>
		<?php if ( has_excerpt()) {?>
		  <a href="<?php echo get_permalink(); ?>"> ... Read more &raquo;</a>
		<?php } ?>
	</section>

  <footer class="article-footer">
    <p class="tags"><?php the_tags('<span class="tags-title">' . __('Tags:', 'jointstheme') . '</span> ', ', ', ''); ?></p>
  </footer>

</article>
