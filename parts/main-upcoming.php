 <div class="large-6 medium-6 small-12 cell">   <div class="front-panel">    <h4>Upcoming Events</h4>    <p>For a complete list of parish events, look at<br/> the <a href="events/parish-calendar">Parish Calendar</a> and the <a href="liturgy-calendar">Liturgy Calendar</a>     </p>    <?php
    echo do_shortcode("[qem posts='5']");
    ?>    To see all upcoming events go to <a title="future" href="upcoming-events">this page</a><br/>
    For a list of past events go to <a title="past"   href="past-events">this page</a>    </p>  </div>  <!-- end of panel--></div>